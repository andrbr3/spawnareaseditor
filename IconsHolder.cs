using UnityEditor;
using UnityEngine;

namespace SpawnAreasEditor
{
    public class IconsHolder
    {
        public Texture2D EyeOff { get; private set; }
        public Texture2D EyeOn{ get; private set; }
        public Texture2D Duplicate{ get; private set; }
        public Texture2D AddRect{ get; private set; }
        public Texture2D AddCircle{ get; private set; }
        public Texture2D Delete{ get; private set; }


        public IconsHolder()
        {
            EyeOn = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Scripts/Editor/SpawnAreasEditor/Icons/EyeActiveOn_Simple_Icons_UI.png");
            EyeOff = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Scripts/Editor/SpawnAreasEditor/Icons/EyeActiveOff_Simple_Icons_UI.png");
            Duplicate = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Scripts/Editor/SpawnAreasEditor/Icons/DuplicateZone.png");
            AddRect = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Scripts/Editor/SpawnAreasEditor/Icons/AddRectZone.png");
            AddCircle = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Scripts/Editor/SpawnAreasEditor/Icons/AddCircleZone.png");
            Delete = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Scripts/Editor/SpawnAreasEditor/Icons/TrashCan_Simple_Icons_UI.png");
        }
    }
}