﻿
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace SpawnAreasEditor
{
    public class SpawnAreasSceneUI
    {
        private bool _showSceneButtons;
        private bool _isHidden = true;
        
        private SpawnAreasTool _tool;
        private IconsHolder _iconsHolder;
        private SpawnAreasToolDataModel _dataModel;


        public SpawnAreasSceneUI(SpawnAreasTool tool, IconsHolder iconsHolder, SpawnAreasToolDataModel model)
        {
            _tool = tool;
            _iconsHolder = iconsHolder;
            _dataModel = model;
        }

        public void OnSceneGUIReapaint(SceneView sceneView)
        {
            if (_isHidden && !_dataModel.ZonesLoaded)
            {
                return;
            }

            Handles.BeginGUI();

            if (GUILayout.Button(new GUIContent(_showSceneButtons ? _iconsHolder.EyeOff : _iconsHolder.EyeOn, "Показать/скрыть инструменты"), GUILayout.Width(30), GUILayout.Height(30)))
            {
                _showSceneButtons = !_showSceneButtons;
            }

            if (_showSceneButtons)
            {
                if (GUILayout.Button(new GUIContent(_iconsHolder.AddCircle, "Добавить круглую зону"), GUILayout.Width(40), GUILayout.Height(40)))
                {
                    _tool.AddZoneCircle();
                }

                if (GUILayout.Button(new GUIContent(_iconsHolder.AddRect, "Добавить прямоугольную зону"), GUILayout.Width(40), GUILayout.Height(40)))
                {
                    _tool.AddZoneRect();
                }

                if (GUILayout.Button(new GUIContent(_iconsHolder.Delete, "Удалить выбранную зону"), GUILayout.Width(40), GUILayout.Height(40)))
                {
                    _tool.RemoveZone();
                }

                if (GUILayout.Button(new GUIContent(_iconsHolder.Duplicate, "Дублировать выбранную зону"), GUILayout.Width(40), GUILayout.Height(40)))
                {
                    _tool.DuplicateZone();
                }
            }

            Handles.EndGUI();
        }

        public void Show() => _isHidden = false;

        public void Hide() => _isHidden = true;
    }
}