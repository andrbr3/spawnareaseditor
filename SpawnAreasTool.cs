using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using CrazyPanda.UnityCore.PandaTasks;
using Newtonsoft.Json;
using Services;
using UnityEngine;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace SpawnAreasEditor
{
    [EditorTool("Edit Spawn Areas")]
    public class SpawnAreasTool : EditorTool
    {
        private SpawnAreasSceneUI _spawnAreasSceneUI;
        private SpawnAreasWindow _spawnAreasWindow;
        private GUIContent _toolbarIcon;
        private ConfigsService _configsService;
        private SpawnAreasToolDataModel _dataModel;
        private SpawnZonesSceneDrawer _spawnZonesSceneDrawer;
        private IconsHolder _iconsHolder;
        
        private void OnEnable()
        {
            _configsService = new ConfigsService();
            _dataModel = new SpawnAreasToolDataModel();
            _iconsHolder = new IconsHolder();
            _spawnAreasSceneUI = new SpawnAreasSceneUI(this,_iconsHolder, _dataModel);
            _spawnZonesSceneDrawer = new SpawnZonesSceneDrawer(this, _dataModel);

            EditorSceneManager.sceneOpened += OnBackClick;
            ToolManager.activeToolChanged += ActiveToolDidChange;
        }

        private void OnDisable()
        {
            _spawnAreasSceneUI?.Hide();
            _spawnZonesSceneDrawer?.Hide();
            _dataModel.ZonesLoaded = false;
            _dataModel.SpawnAreas.Clear();

            EditorSceneManager.sceneOpened -= OnBackClick;
            ToolManager.activeToolChanged -= ActiveToolDidChange;
        }

        private bool TryCopyAreasToDataModel()
        {
            _dataModel.SpawnAreas.Clear();

            var levelConfig = _configsService.LevelsConfig.GetLevelConfig(EditorSceneManager.GetActiveScene().name);
            if (levelConfig == null)
            {
                EditorUtility.DisplayDialog("Error!",
                    "Scene is not present in LevelsConfig.json!", "Ok");
                return false;
            }

            var areas = levelConfig.SpawnAreas;
            foreach (var area in areas)
            {
                var area_copy = CopyArea(area);
                _dataModel.SpawnAreas.Add(area_copy);
            }

            return true;
        }

        private AreaConfig CopyArea(AreaConfig area)
        {
            var area_copy = new AreaConfig()
            {
                Id = area.Id,
                Circle = (area.Circle != null ? new CircleZone() { Pos = new float[3] } : null),
                Rect = (area.Rect != null ? new RectZone() { Pos = new float[3] } : null)
            };

            if (area_copy.Circle != null)
            {
                Array.Copy(area.Circle.Pos, area_copy.Circle.Pos, area.Circle.Pos.Length);
                area_copy.Circle.R = area.Circle.R;
            }

            if (area_copy.Rect != null)
            {
                Array.Copy(area.Rect.Pos, area_copy.Rect.Pos, area.Rect.Pos.Length);
                area_copy.Rect.H = area.Rect.H;
                area_copy.Rect.W = area.Rect.W;
            }

            return area_copy;
        }

        private IPandaTask FetchZones(string configType)
        {
            var task = new PandaTaskCompletionSource();
            _configsService.UpdateConfigs(configType).Done(() =>
            {
                _dataModel.ConfigType = configType;

                if (TryCopyAreasToDataModel())
                {
                    task.Resolve();
                    _dataModel.ZonesLoaded = true;
                }
                else
                {
                    task.CancelTask();
                }
            }).Fail(e =>
            {
                EditorUtility.DisplayDialog("Error!", "Possible connection error! Check VPN!\nDetails: " + e.Message, "ok");
            });

            return task.Task;
        }

        public void LoadZones(string configType)
        {
            FetchZones(configType).Done(() =>
            {
                _dataModel.SelectedZone = 0;
                _spawnAreasSceneUI?.Show();
                _spawnZonesSceneDrawer?.Show();
                EditorFocusOnArea(_dataModel.SpawnAreas[_dataModel.SelectedZone]);
            }).Fail(e =>
            {
                EditorUtility.DisplayDialog("Error!", "Possible connection error! Check VPN!\nDetails: " + e.Message, "ok");
            }).RethrowError(true);
        }

        public void SaveZones()
        {
            var dupZones = FindDuplicateZones();
            if (dupZones.Length > 0)
            {
                string message = "Zones duplicates:\n";
                foreach (var zone in dupZones)
                {
                    message += zone + "\n";
                }

                EditorUtility.DisplayDialog("Error!", message, "Ok");
                return;
            }

            _configsService.UpdateConfigs(_dataModel.ConfigType).Done(() =>
            {
                var config = _configsService.LevelsConfig.GetLevelConfig(EditorSceneManager.GetActiveScene().name);

                var areas = config.SpawnAreas;
                areas.Clear();
                foreach (var area in _dataModel.SpawnAreas)
                {
                    var area_copy = CopyArea(area);
                    areas.Add(area_copy);
                }

                var json = JsonConvert.SerializeObject(_configsService.LevelsConfig, Formatting.Indented);
                _configsService.UpdateJsonFile("LevelsConfig.json", _dataModel.ConfigType, json);

                EditorUtility.DisplayDialog("Success!",
                    "LevelsConfig for " + _dataModel.ConfigType + " updated!", "Ok");

                OnBackClick(new Scene(), OpenSceneMode.Additive);

            }).Fail(e =>
            {
                EditorUtility.DisplayDialog("Error!",
                    e.Message, "Ok");
            });
        }

        public void OnBackClick(Scene scene, OpenSceneMode mode)
        {
            _dataModel.ZonesLoaded = false;
            _dataModel.SpawnAreas.Clear();
            _spawnZonesSceneDrawer?.Hide();
        }

        private void ActiveToolDidChange()
        {
            if (!ToolManager.IsActiveTool(this))
            {
                if (_spawnAreasWindow != null)
                {
                    _dataModel.ZonesLoaded = false;
                    _dataModel.SpawnAreas.Clear();
                    _spawnAreasWindow.Close();
                    _spawnAreasWindow = null;
                }

                _spawnAreasSceneUI?.Hide();
                _spawnZonesSceneDrawer?.Hide();
                SceneView.duringSceneGui -= _spawnAreasSceneUI.OnSceneGUIReapaint;
                SceneView.duringSceneGui -= _spawnZonesSceneDrawer.OnSceneGUIReapaint;

                return;
            }

            _spawnAreasWindow = EditorWindow.GetWindow(typeof(SpawnAreasWindow), false, "Spawn areas editor") as SpawnAreasWindow;
            _spawnAreasWindow.ShowUtility();

            _spawnAreasWindow.Init(this, _dataModel, _iconsHolder);
            SceneView.duringSceneGui += _spawnZonesSceneDrawer.OnSceneGUIReapaint;
            SceneView.duringSceneGui += _spawnAreasSceneUI.OnSceneGUIReapaint;
        }

        public override void OnToolGUI(EditorWindow window)
        {
        }

        public void AddZoneCircle()
        {
            var sceneCam = SceneView.lastActiveSceneView.camera;
            var pos = sceneCam.transform.position + sceneCam.transform.forward * 1;
            var areaNew = new AreaConfig()
            {
                Id = "New_Zone" + _dataModel.SelectedZone,
                Circle = new CircleZone()
                {
                    Pos = new[] { pos.x, pos.y, pos.z },
                    R = 2
                }
            };

            if (_dataModel.SpawnAreas.Count > 0)
            {
                _dataModel.SpawnAreas.Insert(_dataModel.SelectedZone, areaNew);
            }
            else
            {
                _dataModel.SpawnAreas.Add(areaNew);
            }

            EditorFocusOnArea(areaNew);
            _spawnAreasWindow.Repaint();
        }

        public void AddZoneRect()
        {
            var sceneCam = SceneView.lastActiveSceneView.camera;
            var pos = sceneCam.transform.position + sceneCam.transform.forward * 1;
            var areaNew = new AreaConfig()
            {
                Id = "New_Zone" + _dataModel.SelectedZone,
                Rect = new RectZone()
                {
                    Pos = new[] { pos.x, pos.y, pos.z },
                    W = 2,
                    H = 2
                }
            };

            if (_dataModel.SpawnAreas.Count > 0)
            {
                _dataModel.SpawnAreas.Insert(_dataModel.SelectedZone, areaNew);
            }
            else
            {
                _dataModel.SpawnAreas.Add(areaNew);
            }

            EditorFocusOnArea(areaNew);
            _spawnAreasWindow.Repaint();
        }

        public void DuplicateZone()
        {
            if (_dataModel.SpawnAreas.Count == 0)
            {
                return;
            }

            var selectedZone = _dataModel.SpawnAreas[_dataModel.SelectedZone];
            var areaCopy = CopyArea(selectedZone);
            var posArray = areaCopy.GetPositionArray();
            _dataModel.SpawnAreas.Insert(_dataModel.SelectedZone, areaCopy);
            posArray[0] += 0.5f;
            posArray[1] += 0.5f;
            areaCopy.Id = "New_Zone" + _dataModel.SelectedZone;
            EditorFocusOnArea(areaCopy);
            _spawnAreasWindow.Repaint();
        }

        public void RemoveZone()
        {
            if (_dataModel.SpawnAreas.Count == 0)
            {
                return;
            }

            _dataModel.SpawnAreas.RemoveAt(_dataModel.SelectedZone);
            _dataModel.SelectedZone = Mathf.Max(Mathf.Clamp(_dataModel.SelectedZone, 0, _dataModel.SpawnAreas.Count - 1), 0);
            _spawnAreasWindow.Repaint();
        }

        public override GUIContent toolbarIcon
        {
            get
            {
                if (_toolbarIcon == null)
                {
                    _toolbarIcon = new GUIContent(
                        AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Art/UI/Inventory/icon_drop.png"),
                        "Spawn Areas Tool");
                }

                return _toolbarIcon;
            }
        }

        public void EditorRepaintWindow()
        {
            _spawnAreasWindow.Repaint();
        }

        public void EditorFocusOnArea(AreaConfig area)
        {
            var pos = area.GetPosition();
            Bounds b = new Bounds();
            b.center = area.Circle != null ? pos : pos + new Vector3(area.Rect.W / 2, area.Rect.H / 2);
            b.size = new Vector3(area.Circle?.R * 2 ?? area.Rect.W, 1f, area.Circle?.R * 2 ?? area.Rect.H);
            SceneView.lastActiveSceneView.Frame(b, false);
        }

        public bool IsModelChanged()
        {
            var areas = _configsService.LevelsConfig.GetLevelConfig(EditorSceneManager.GetActiveScene().name).SpawnAreas;
            if (_dataModel.SpawnAreas.Count != areas.Count)
            {
                return true;
            }

            for (int i = 0; i < areas.Count; i++)
            {
                if (!areas[i].CompareTo(_dataModel.SpawnAreas[i]))
                {
                    return true;
                }
            }

            return false;
        }

        private string[] FindDuplicateZones()
        {
            return _dataModel.SpawnAreas
                .GroupBy(i => i.Id)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key).ToArray();
        }
    }
}