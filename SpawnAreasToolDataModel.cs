﻿using System.Collections.Generic;

namespace SpawnAreasEditor
{
    public class SpawnAreasToolDataModel
    {
        public List<AreaConfig> SpawnAreas = new List<AreaConfig>();
        public string ConfigType;
        public int SelectedZone;
        public bool ZonesLoaded;
    }
}