using System;
using System.Collections.Generic;
using Services;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpawnAreasEditor
{
    public class SpawnAreasWindow : EditorWindow
    {
        private Vector2 _scrollPos;
        private string[] _availableConfigs;
        private int _selectedConfig;
        private SpawnAreasTool _spawnTool;
        private SpawnAreasToolDataModel _dataModel;
        private IconsHolder _iconsHolder;

        private void Awake()
        {
            _availableConfigs = new string[] { };
        }

        public void Init(SpawnAreasTool tool, SpawnAreasToolDataModel model, IconsHolder iconsHolder)
        {
            EditorStyles.popup.alignment = TextAnchor.MiddleCenter;
            _spawnTool = tool;
            _dataModel = model;
            _iconsHolder = iconsHolder;

            var configsService = new ConfigsService();
            configsService.UpdatePresets()
                .Done(() => _availableConfigs = configsService.ConfigsPresets.ToArray())
                .Fail(e =>
                {
                    EditorUtility.DisplayDialog("Error!", "Possible connection error! Check VPN!\nDetails: " + e.Message, "ok");
                });
        }

        private void OnGUI()
        {
            GUILayout.Label("Config selection:");
            if (_availableConfigs.Length > 0 && _dataModel != null)
            {
                GUILayout.BeginHorizontal();

                GUI.enabled = !_dataModel.ZonesLoaded;
                _selectedConfig = EditorGUILayout.Popup(_selectedConfig, _availableConfigs);
                GUI.enabled = true;

                if (_dataModel.ZonesLoaded)
                {
                    if (GUILayout.Button("Back"))
                    {
                        if (_spawnTool.IsModelChanged())
                        {
                            if (!EditorUtility.DisplayDialog("Model chandged!", "Do you really want to lose you progress?", "Ok",
                                    "Cancel"))
                            {
                                return;
                            }
                        }

                        _spawnTool.OnBackClick(new Scene(), OpenSceneMode.Single);
                    }
                }

                if (!_dataModel.ZonesLoaded)
                {
                    if (GUILayout.Button("Load config"))
                    {
                        _spawnTool.LoadZones(_availableConfigs[_selectedConfig]);
                    }
                }

                if (_dataModel.ZonesLoaded)
                {
                    if (GUILayout.Button("Save config"))
                    {
                        _spawnTool.SaveZones();
                    }
                }

                GUILayout.EndHorizontal();
            }

            GUILayout.Label("Spawn zones:");
            EditorGUILayout.BeginVertical();
            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
            if (_dataModel != null)
            {
                var areas = _dataModel.SpawnAreas;
                for (int i = 0; i < areas?.Count; i++)
                {
                    GUILayout.BeginHorizontal(_dataModel.SelectedZone == i ? BackgroundStyle.Get(Color.red) : BackgroundStyle.Get(Color.gray));
                    areas[i].Id = GUILayout.TextField(areas[i].Id);

                    if (GUILayout.Button(_iconsHolder.EyeOn, GUILayout.Width(30), GUILayout.Height(30)))
                    {
                        _dataModel.SelectedZone = i;
                        _spawnTool.EditorFocusOnArea(areas[i]);
                    }

                    GUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();


        }
    }

    public static class BackgroundStyle
    {
        private static GUIStyle style = new GUIStyle();
        private static Texture2D texture = new Texture2D(1, 1);

        public static GUIStyle Get(Color color)
        {
            if (texture == null)
            {
                texture = new Texture2D(1, 1);
            }

            if (style == null)
            {
                style = new GUIStyle();
            }

            texture.SetPixel(0, 0, color);
            texture.Apply();
            style.normal.background = texture;
            return style;
        }
    }
}