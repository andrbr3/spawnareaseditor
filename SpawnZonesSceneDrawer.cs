﻿
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace SpawnAreasEditor
{
    public class SpawnZonesSceneDrawer
    {
        private bool _isHidden = true;
        private SpawnAreasToolDataModel _dataModel;
        private SpawnAreasTool _tool;
        private Color _selectedColor = new Color(1f, 0f, 0f, 0.3f);
        private Color _unSelectedColor = new Color(1f, 1f, 1f, 0.3f);
        private GUIStyle _textStyle = new GUIStyle();
        private Vector3[] _rectVerts;

        public SpawnZonesSceneDrawer(SpawnAreasTool tool, SpawnAreasToolDataModel model)
        {
            _dataModel = model;
            _tool = tool;

            _textStyle.alignment = TextAnchor.MiddleCenter;
            _rectVerts = new Vector3[4];
        }

        public void OnSceneGUIReapaint(SceneView sceneView)
        {
            if (_isHidden)
            {
                return;
            }

            Handles.zTest = CompareFunction.Less;

            var areas = _dataModel.SpawnAreas;
            
            for (int i = 0; i < areas.Count; i++)
            {
                if (i == _dataModel.SelectedZone)
                {
                    continue;
                }
                
                DrawShape(areas[i], i);
            }
            
            DrawShape(areas[_dataModel.SelectedZone], _dataModel.SelectedZone);

            Handles.zTest = CompareFunction.Always;
        }

        private void DrawShape(AreaConfig config, int index)
        {
            var pos = config.GetPosition();
            if (config.Circle != null)
            {
                Handles.color = _dataModel.SelectedZone == index ? _selectedColor : _unSelectedColor;
                Handles.DrawSolidDisc(pos, Vector3.up, config.Circle.R);
                if (Handles.Button(pos, Quaternion.Euler(90, 0, 0), config.Circle.R, config.Circle.R,
                        Handles.CircleHandleCap))
                {
                    _dataModel.SelectedZone = index;
                    _tool.EditorRepaintWindow();
                }

                Handles.color = Color.white;
                if (index == _dataModel.SelectedZone)
                {
                    config.Circle.R = Mathf.Max(Handles.ScaleValueHandle(config.Circle.R,
                        pos + Vector3.forward * config.Circle.R,
                        Quaternion.identity,
                        2.0f,
                        Handles.CubeHandleCap,
                        1.0f), 0.1f);

                    var handlePos = Handles.PositionHandle(pos, Quaternion.identity);
                    config.SetPosition(handlePos);
                }
                
                DrawTextLabel(_dataModel.SelectedZone == index ? _selectedColor : _unSelectedColor, 
                    pos,
                    config.Id);
            }
            else
            {
                _rectVerts[0] = new Vector3(pos.x, pos.y, pos.z);
                _rectVerts[1] = new Vector3(pos.x + config.Rect.W, pos.y, pos.z);
                _rectVerts[2] = new Vector3(pos.x + config.Rect.W, pos.y, pos.z + config.Rect.H);
                _rectVerts[3] = new Vector3(pos.x, pos.y, pos.z + config.Rect.H);

                Handles.DrawSolidRectangleWithOutline(_rectVerts, _dataModel.SelectedZone == index ? _selectedColor : _unSelectedColor, Color.black);
                if (Handles.Button(pos + new Vector3(config.Rect.W / 2, 0f, config.Rect.H / 2),
                        Quaternion.Euler(90, 0, 0), config.Rect.H / 2, config.Rect.H / 2,
                        Handles.RectangleHandleCap))
                {
                    _dataModel.SelectedZone = index;
                    _tool.EditorRepaintWindow();
                }

                if (index == _dataModel.SelectedZone)
                {
                    config.Rect.W = Mathf.Max(Handles.ScaleValueHandle(config.Rect.W,
                        new Vector3(pos.x + config.Rect.W, pos.y, pos.z + config.Rect.H / 2), 
                        Quaternion.identity,
                        2.0f,
                        Handles.CubeHandleCap,
                        1.0f), 0.1f);
                    
                    config.Rect.H = Mathf.Max(Handles.ScaleValueHandle(config.Rect.H,
                        new Vector3(pos.x + config.Rect.W / 2, pos.y, pos.z + config.Rect.H), 
                        Quaternion.identity,
                        2.0f,
                        Handles.CubeHandleCap,
                        1.0f), 0.1f);

                    var centerPos = Handles.PositionHandle(pos, Quaternion.identity);
                    config.SetPosition(centerPos);
                }

                DrawTextLabel(_dataModel.SelectedZone == index ? _selectedColor : _unSelectedColor, 
                    new Vector3(pos.x + config.Rect.W / 2, pos.y, pos.z + config.Rect.H / 2),
                    config.Id);
            }
        }

        private void DrawTextLabel(Color color, Vector3 pos, string text)
        {
            Handles.color = color;
            Handles.Label(pos, 
                text, _textStyle);
            Handles.color = Color.white;
        }

        public void Show() => _isHidden = false;
        public void Hide() => _isHidden = true;
    }
}